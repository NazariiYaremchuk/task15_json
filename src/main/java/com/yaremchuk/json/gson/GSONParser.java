package com.yaremchuk.json.gson;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.yaremchuk.model.Device;

import java.io.*;
import java.util.Arrays;
import java.util.List;

public class GSONParser {
    private Gson gson;

    public GSONParser() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gson = gsonBuilder.create();
    }

    public List<Device> getDevices(File json) throws FileNotFoundException {
        List<Device> devices;
        devices = Arrays.asList(gson.fromJson(new FileReader(json), Device[].class));
        return devices;
    }

    public void writeJson(File json, List<Device>devices) throws IOException {

        try (FileWriter fileWriter = new FileWriter(json);){
            gson.toJson(json, fileWriter);
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }
}
