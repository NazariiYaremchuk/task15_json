package com.yaremchuk.json.validators;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.fge.jackson.JsonLoader;
import com.github.fge.jsonschema.core.exceptions.ProcessingException;
import com.github.fge.jsonschema.core.report.ProcessingReport;
import com.github.fge.jsonschema.main.JsonSchemaFactory;
import com.github.fge.jsonschema.main.JsonValidator;

import java.io.File;
import java.io.IOException;

public class FirstValidator {
    public static boolean validate(File json, File schema) {
        try {
            final JsonNode data = JsonLoader.fromFile(json);
            final JsonNode schemaData = JsonLoader.fromFile(schema);
            final JsonSchemaFactory factory = JsonSchemaFactory.byDefault();
            JsonValidator jsonValidator = factory.getValidator();
            ProcessingReport report = jsonValidator.validate(schemaData, data);
            return report.isSuccess();
        } catch (IOException | ProcessingException e) {
            e.printStackTrace();
        }
        return false;
    }
}
