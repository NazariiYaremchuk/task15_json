package com.yaremchuk.model;

public class Type {
    boolean isPerifery;
    double energyConsumption;
    boolean isCooler;
    ComplectingGroup complectingGroup;

    public Type(boolean isPerifery, double energyConsumption, boolean isCooler, ComplectingGroup complectingGroup) {
        this.isPerifery = isPerifery;
        this.energyConsumption = energyConsumption;
        this.isCooler = isCooler;
        this.complectingGroup = complectingGroup;
    }

    @Override
    public String toString() {
        return "Type{" +
                "isPerifery=" + isPerifery +
                ", energyConsumption=" + energyConsumption +
                ", isCooler=" + isCooler +
                ", complectingGroup=" + complectingGroup +
                '}';
    }
}
