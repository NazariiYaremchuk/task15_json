package com.yaremchuk.model;

public class ComplectingGroup {
    DeviceGroup deviceGroup;
    Ports ports;

    public ComplectingGroup(DeviceGroup deviceGroup, Ports ports) {
        this.deviceGroup = deviceGroup;
        this.ports = ports;
    }

    @Override
    public String toString() {
        return "ComplectingGroup{" +
                "deviceGroup=" + deviceGroup +
                ", ports=" + ports +
                '}';
    }
}
