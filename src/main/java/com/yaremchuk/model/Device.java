package com.yaremchuk.model;

import java.util.Comparator;

public class Device implements Comparator<Device> {
    int id;
    String name;
    String origin;
    Integer price;
    Type type;
    boolean isCritical;

    public Device(int id, String name, String origin, Integer price, Type type, boolean isCritical) {
        this.id = id;
        this.name = name;
        this.origin = origin;
        this.price = price;
        this.type = type;
        this.isCritical = isCritical;
    }

    public Device() {

    }

    @Override
    public String toString() {
        return "Device{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", origin='" + origin + '\'' +
                ", price=" + price +
                ", type=" + type +
                ", isCritical=" + isCritical +
                '}';
    }


    @Override
    public int compare(Device o1, Device o2) {
        return (o1.price - o2.price);
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setCritical(boolean critical) {
        isCritical = critical;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public void setId(int id) {
        this.id = id;
    }
}

